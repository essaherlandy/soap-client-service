package models

import "encoding/xml"

type SingleRequest struct {
	XMLName      xml.Name     `xml:"soapenv:Envelope"`
	XmlnsSoapenv string       `xml:"xmlns:soapenv,attr"`
	XmlnsCb5     string       `xml:"xmlns:cb5,attr"`
	XmlnsSmar    string       `xml:"xmlns:smar,attr"`
	Header       SingleHeader `xml:"soapenv:Header"`
	Body         SingleBody   `xml:"soapenv:Body"`
}

type SingleHeader struct{}

type SingleBody struct {
	SmartSearchIndividual SingleSmartSearchIndividual `xml:"cb5:SmartSearchIndividual"`
}

type SingleSmartSearchIndividual struct {
	Query SingleQuery `xml:"cb5:query"`
}

type SingleQuery struct {
	InquiryReason string           `xml:"smar:InquiryReason"`
	Parameters    SingleParameters `xml:"smar:Parameters"`
	ReferenceCode string           `xml:"smar:ReferenceCode"`
}

type SingleParameters struct {
	DateOfBirth string          `xml:"smar:DateOfBirth"`
	FullName    string          `xml:"smar:FullName"`
	IdNumbers   SingleIdNumbers `xml:"smar:IdNumbers"`
}

type SingleIdNumbers struct {
	IdNumberPairIndividual SingleIdNumberPairIndividual `xml:"smar:IdNumberPairIndividual"`
}

type SingleIdNumberPairIndividual struct {
	IdNumber     string `xml:"smar:IdNumber"`
	IdNumberType string `xml:"smar:IdNumberType"`
}
