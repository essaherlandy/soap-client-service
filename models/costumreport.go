package models

import "encoding/xml"

type CostumReportRequest struct {
	XMLName      xml.Name           `xml:"soapenv:Envelope"`
	XmlnsSoapenv string             `xml:"xmlns:soapenv,attr"`
	XmlnsCb5     string             `xml:"xmlns:cb5,attr"`
	XmlnsCus     string             `xml:"xmlns:cus,attr"`
	XmlnsArr     string             `xml:"xmlns:arr,attr"`
	CostumHeader CostumReportHeader `xml:"soapenv:Header"`
	CostumBody   CostumReportBody   `xml:"soapenv:Body"`
}

type CostumReportHeader struct{}

type CostumReportBody struct {
	GetCustomReport GetCustomReport `xml:"cb5:GetCustomReport"`
}

type GetCustomReport struct {
	Parameters Parameters `xml:"cb5:parameters"`
}

type Parameters struct {
	Consent                   bool           `xml:"cus:Consent"`
	CostumReportIDNumber      string         `xml:"cus:IDNumber"`
	CostumReportIDNumberType  string         `xml:"cus:IDNumberType"`
	CostumReportInquiryReason string         `xml:"cus:InquiryReason"`
	CostumReportReferenceCode string         `xml:"cus:ReferenceCode"`
	CostumReportReportDate    string         `xml:"cus:ReportDate"`
	CostumSections            CostumSections `xml:"cus:Sections"`
	SubjectType               string         `xml:"cus:SubjectType"`
}

type CostumSections struct {
	String []string `xml:"arr:string"`
}
