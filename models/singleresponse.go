package models

import "encoding/xml"

// Response structs
type SingleResponse struct {
	XMLName xml.Name     `xml:"Envelope"`
	Body    ResponseBody `xml:"Body"`
}

type ResponseBody struct {
	SmartSearchIndividualResponse ResponseSmartSearchIndividualResponse `xml:"SmartSearchIndividualResponse"`
}

type ResponseSmartSearchIndividualResponse struct {
	SmartSearchIndividualResult ResponseSmartSearchIndividualResult `xml:"SmartSearchIndividualResult"`
}

type ResponseSmartSearchIndividualResult struct {
	IdScoreId         string                    `xml:"IdScoreId"`
	IndividualRecords ResponseIndividualRecords `xml:"IndividualRecords"`
	NilReport         ResponseNilReport         `xml:"NilReport"`
	Parameters        ResponseParameters        `xml:"Parameters"`
	SearchRuleApplied string                    `xml:"SearchRuleApplied"`
	Status            string                    `xml:"Status"`
}

type ResponseIndividualRecords struct {
	SearchIndividualRecord ResponseSearchIndividualRecord `xml:"SearchIndividualRecord"`
}

type ResponseSearchIndividualRecord struct {
	Address     string `xml:"Address"`
	DateOfBirth string `xml:"DateOfBirth"`
	FullName    string `xml:"FullName"`
	IdScoreId   string `xml:"IdScoreId"`
	KTP         string `xml:"KTP"`
}

type ResponseNilReport struct {
	Nil bool `xml:"nil,attr"`
}

type ResponseParameters struct {
	DateOfBirth string            `xml:"DateOfBirth"`
	FullName    string            `xml:"FullName"`
	IdNumbers   ResponseIdNumbers `xml:"IdNumbers"`
}

type ResponseIdNumbers struct {
	IdNumberPairIndividual ResponseIdNumberPairIndividual `xml:"IdNumberPairIndividual"`
}

type ResponseIdNumberPairIndividual struct {
	IdNumber     string `xml:"IdNumber"`
	IdNumberType string `xml:"IdNumberType"`
}
