package models

import "encoding/xml"

type ResponseReportData struct {
	XMLName xml.Name `xml:"Envelope"`
	Text    string   `xml:",chardata"`
	S       string   `xml:"s,attr"`
	Body    struct {
		Text                    string `xml:",chardata"`
		GetCustomReportResponse struct {
			Text                  string `xml:",chardata"`
			Xmlns                 string `xml:"xmlns,attr"`
			GetCustomReportResult struct {
				Text string `xml:",chardata"`
				CIP  struct {
					Text       string `xml:",chardata"`
					B          string `xml:"b,attr"`
					RecordList struct {
						Text   string `xml:",chardata"`
						Record []struct {
							Text                 string `xml:",chardata"`
							Date                 string `xml:"Date"`
							Grade                string `xml:"Grade"`
							ProbabilityOfDefault string `xml:"ProbabilityOfDefault"`
							ReasonsList          struct {
								Text   string `xml:",chardata"`
								Reason []struct {
									Text        string `xml:",chardata"`
									Code        string `xml:"Code"`
									Description struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"Description"`
								} `xml:"Reason"`
							} `xml:"ReasonsList"`
							Score string `xml:"Score"`
							Trend string `xml:"Trend"`
						} `xml:"Record"`
					} `xml:"RecordList"`
				} `xml:"CIP"`
				CIQ struct {
					Text   string `xml:",chardata"`
					B      string `xml:"b,attr"`
					Detail struct {
						Text                                       string `xml:",chardata"`
						NumberOfCancelledClosedContracts           string `xml:"NumberOfCancelledClosedContracts"`
						NumberOfSubscribersMadeInquiriesLast14Days string `xml:"NumberOfSubscribersMadeInquiriesLast14Days"`
						NumberOfSubscribersMadeInquiriesLast2Days  string `xml:"NumberOfSubscribersMadeInquiriesLast2Days"`
					} `xml:"Detail"`
					Summary struct {
						Text                                      string `xml:",chardata"`
						DateOfLastFraudRegistrationPrimarySubject string `xml:"DateOfLastFraudRegistrationPrimarySubject"`
						DateOfLastFraudRegistrationThirdParty     struct {
							Text string `xml:",chardata"`
							Nil  string `xml:"nil,attr"`
						} `xml:"DateOfLastFraudRegistrationThirdParty"`
						NumberOfFraudAlertsPrimarySubject string `xml:"NumberOfFraudAlertsPrimarySubject"`
						NumberOfFraudAlertsThirdParty     string `xml:"NumberOfFraudAlertsThirdParty"`
					} `xml:"Summary"`
				} `xml:"CIQ"`
				Company struct {
					Text string `xml:",chardata"`
					Nil  string `xml:"nil,attr"`
					B    string `xml:"b,attr"`
				} `xml:"Company"`
				ContractOverview struct {
					Text         string `xml:",chardata"`
					B            string `xml:"b,attr"`
					ContractList struct {
						Text     string `xml:",chardata"`
						Contract []struct {
							Text              string `xml:",chardata"`
							ContractStatus    string `xml:"ContractStatus"`
							OutstandingAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"OutstandingAmount"`
							PastDueAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"PastDueAmount"`
							PastDueDays     string `xml:"PastDueDays"`
							PhaseOfContract string `xml:"PhaseOfContract"`
							RoleOfClient    string `xml:"RoleOfClient"`
							Sector          string `xml:"Sector"`
							StartDate       string `xml:"StartDate"`
							TotalAmount     struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"TotalAmount"`
							TypeOfContract string `xml:"TypeOfContract"`
						} `xml:"Contract"`
					} `xml:"ContractList"`
				} `xml:"ContractOverview"`
				ContractSummary struct {
					Text   string `xml:",chardata"`
					B      string `xml:"b,attr"`
					Debtor struct {
						Text                 string `xml:",chardata"`
						ClosedContracts      string `xml:"ClosedContracts"`
						OpenContracts        string `xml:"OpenContracts"`
						OutstandingAmountSum struct {
							Text       string `xml:",chardata"`
							C          string `xml:"c,attr"`
							Currency   string `xml:"Currency"`
							LocalValue string `xml:"LocalValue"`
							Value      string `xml:"Value"`
						} `xml:"OutstandingAmountSum"`
						PastDueAmountSum struct {
							Text       string `xml:",chardata"`
							C          string `xml:"c,attr"`
							Currency   string `xml:"Currency"`
							LocalValue string `xml:"LocalValue"`
							Value      string `xml:"Value"`
						} `xml:"PastDueAmountSum"`
						TotalAmountSum struct {
							Text       string `xml:",chardata"`
							C          string `xml:"c,attr"`
							Currency   string `xml:"Currency"`
							LocalValue string `xml:"LocalValue"`
							Value      string `xml:"Value"`
						} `xml:"TotalAmountSum"`
					} `xml:"Debtor"`
					Guarantor struct {
						Text                 string `xml:",chardata"`
						ClosedContracts      string `xml:"ClosedContracts"`
						OpenContracts        string `xml:"OpenContracts"`
						OutstandingAmountSum struct {
							Text string `xml:",chardata"`
							Nil  string `xml:"nil,attr"`
							C    string `xml:"c,attr"`
						} `xml:"OutstandingAmountSum"`
						PastDueAmountSum struct {
							Text string `xml:",chardata"`
							Nil  string `xml:"nil,attr"`
							C    string `xml:"c,attr"`
						} `xml:"PastDueAmountSum"`
						TotalAmountSum struct {
							Text string `xml:",chardata"`
							Nil  string `xml:"nil,attr"`
							C    string `xml:"c,attr"`
						} `xml:"TotalAmountSum"`
					} `xml:"Guarantor"`
					Overall struct {
						Text                      string `xml:",chardata"`
						LastDelinquency90PlusDays string `xml:"LastDelinquency90PlusDays"`
						WorstPastDueAmount        struct {
							Text       string `xml:",chardata"`
							C          string `xml:"c,attr"`
							Currency   string `xml:"Currency"`
							LocalValue string `xml:"LocalValue"`
							Value      string `xml:"Value"`
						} `xml:"WorstPastDueAmount"`
						WorstPastDueDays string `xml:"WorstPastDueDays"`
					} `xml:"Overall"`
					PaymentCalendarList struct {
						Text            string `xml:",chardata"`
						PaymentCalendar []struct {
							Text                     string `xml:",chardata"`
							ContractsSubmitted       string `xml:"ContractsSubmitted"`
							Date                     string `xml:"Date"`
							NegativeStatusOfContract string `xml:"NegativeStatusOfContract"`
							OutstandingAmount        struct {
								Text       string `xml:",chardata"`
								Nil        string `xml:"nil,attr"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"OutstandingAmount"`
							PastDueAmount struct {
								Text       string `xml:",chardata"`
								Nil        string `xml:"nil,attr"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"PastDueAmount"`
							PastDueDays struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"PastDueDays"`
						} `xml:"PaymentCalendar"`
					} `xml:"PaymentCalendarList"`
					SectorInfoList struct {
						Text       string `xml:",chardata"`
						SectorInfo []struct {
							Text                       string `xml:",chardata"`
							DebtorClosedContracts      string `xml:"DebtorClosedContracts"`
							DebtorOpenContracts        string `xml:"DebtorOpenContracts"`
							DebtorOutstandingAmountSum struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"DebtorOutstandingAmountSum"`
							DebtorPastDueAmountSum struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"DebtorPastDueAmountSum"`
							DebtorTotalAmountSum struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"DebtorTotalAmountSum"`
							GuarantorClosedContracts      string `xml:"GuarantorClosedContracts"`
							GuarantorOpenContracts        string `xml:"GuarantorOpenContracts"`
							GuarantorOutstandingAmountSum struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
								C    string `xml:"c,attr"`
							} `xml:"GuarantorOutstandingAmountSum"`
							GuarantorPastDueAmountSum struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
								C    string `xml:"c,attr"`
							} `xml:"GuarantorPastDueAmountSum"`
							GuarantorTotalAmountSum struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
								C    string `xml:"c,attr"`
							} `xml:"GuarantorTotalAmountSum"`
							Sector string `xml:"Sector"`
						} `xml:"SectorInfo"`
					} `xml:"SectorInfoList"`
				} `xml:"ContractSummary"`
				Contracts struct {
					Text         string `xml:",chardata"`
					B            string `xml:"b,attr"`
					ContractList struct {
						Text     string `xml:",chardata"`
						Contract []struct {
							Text            string `xml:",chardata"`
							BankBeneficiary struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"BankBeneficiary"`
							Branch struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"Branch"`
							CollateralsList struct {
								Text       string `xml:",chardata"`
								Collateral struct {
									Text           string `xml:",chardata"`
									AppraisalValue struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
										C    string `xml:"c,attr"`
									} `xml:"AppraisalValue"`
									BankValuationDate struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"BankValuationDate"`
									BankValue struct {
										Text       string `xml:",chardata"`
										C          string `xml:"c,attr"`
										Currency   string `xml:"Currency"`
										LocalValue string `xml:"LocalValue"`
										Value      string `xml:"Value"`
									} `xml:"BankValue"`
									Branch struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"Branch"`
									CollateralAcceptanceDate struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"CollateralAcceptanceDate"`
									CollateralAppraisalAuthority struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"CollateralAppraisalAuthority"`
									CollateralCode        string `xml:"CollateralCode"`
									CollateralDescription string `xml:"CollateralDescription"`
									CollateralOwnerName   string `xml:"CollateralOwnerName"`
									CollateralRating      struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"CollateralRating"`
									CollateralStatus string `xml:"CollateralStatus"`
									CollateralType   string `xml:"CollateralType"`
									CollateralValue  struct {
										Text       string `xml:",chardata"`
										C          string `xml:"c,attr"`
										Currency   string `xml:"Currency"`
										LocalValue string `xml:"LocalValue"`
										Value      string `xml:"Value"`
									} `xml:"CollateralValue"`
									HasMultipleCollaterals string `xml:"HasMultipleCollaterals"`
									Insurance              string `xml:"Insurance"`
									IsShared               string `xml:"IsShared"`
									MainAddressAddressLine struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"MainAddressAddressLine"`
									MainAddressCity struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"MainAddressCity"`
									MainAddressStreet struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"MainAddressStreet"`
									ProofOfOwnership struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"ProofOfOwnership"`
									RatingAuthority        string `xml:"RatingAuthority"`
									SecurityAssignmentType string `xml:"SecurityAssignmentType"`
									SharedPortion          struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"SharedPortion"`
									ValuationDate string `xml:"ValuationDate"`
								} `xml:"Collateral"`
							} `xml:"CollateralsList"`
							ConditionDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"ConditionDate"`
							ContractCode            string `xml:"ContractCode"`
							ContractCurrency        string `xml:"ContractCurrency"`
							ContractStatus          string `xml:"ContractStatus"`
							ContractSubtype         string `xml:"ContractSubtype"`
							ContractType            string `xml:"ContractType"`
							CreditCharacteristics   string `xml:"CreditCharacteristics"`
							CreditClassification    string `xml:"CreditClassification"`
							CreditUsageInLast30Days struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
								C    string `xml:"c,attr"`
							} `xml:"CreditUsageInLast30Days"`
							Creditor     string `xml:"Creditor"`
							CreditorType string `xml:"CreditorType"`
							DefaultDate  struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"DefaultDate"`
							DefaultReason            string `xml:"DefaultReason"`
							DefaultReasonDescription struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"DefaultReasonDescription"`
							DelinquencyDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"DelinquencyDate"`
							Description struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"Description"`
							DisbursementDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"DisbursementDate"`
							Disputes struct {
								Text                  string `xml:",chardata"`
								ActiveDisputes        string `xml:"ActiveDisputes"`
								ActiveInCourtDisputes string `xml:"ActiveInCourtDisputes"`
								ClosedDisputes        string `xml:"ClosedDisputes"`
								DisputeList           string `xml:"DisputeList"`
								FalseDisputes         string `xml:"FalseDisputes"`
							} `xml:"Disputes"`
							EconomicSector    string `xml:"EconomicSector"`
							GovernmentProgram string `xml:"GovernmentProgram"`
							GuarantyDeposit   struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"GuarantyDeposit"`
							InitialAgreementDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"InitialAgreementDate"`
							InitialAgreementNumber struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"InitialAgreementNumber"`
							InitialInterestRate      string `xml:"InitialInterestRate"`
							InitialInterestRateType  string `xml:"InitialInterestRateType"`
							InitialRestructuringDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"InitialRestructuringDate"`
							InitialTotalAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"InitialTotalAmount"`
							InterestArrears struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"InterestArrears"`
							InterestArrearsFrequency struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"InterestArrearsFrequency"`
							LastAgreementDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"LastAgreementDate"`
							LastAgreementNumber struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"LastAgreementNumber"`
							LastDelinquency90PlusDays struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"LastDelinquency90PlusDays"`
							LastInterestRate     string `xml:"LastInterestRate"`
							LastInterestRateType string `xml:"LastInterestRateType"`
							LastUpdate           string `xml:"LastUpdate"`
							MaturityDate         struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"MaturityDate"`
							NameOfInsured struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"NameOfInsured"`
							NegativeStatusOfContract string `xml:"NegativeStatusOfContract"`
							OrientationOfUse         string `xml:"OrientationOfUse"`
							OutstandingAmount        struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"OutstandingAmount"`
							PastDueAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"PastDueAmount"`
							PastDueDays     string `xml:"PastDueDays"`
							PastDueInterest struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"PastDueInterest"`
							PaymentCalendarList struct {
								Text         string `xml:",chardata"`
								CalendarItem []struct {
									Text              string `xml:",chardata"`
									Date              string `xml:"Date"`
									DelinquencyStatus string `xml:"DelinquencyStatus"`
									InterestRate      struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"InterestRate"`
									NegativeStatusOfContract string `xml:"NegativeStatusOfContract"`
									OutstandingAmount        struct {
										Text       string `xml:",chardata"`
										Nil        string `xml:"nil,attr"`
										C          string `xml:"c,attr"`
										Currency   string `xml:"Currency"`
										LocalValue string `xml:"LocalValue"`
										Value      string `xml:"Value"`
									} `xml:"OutstandingAmount"`
									PastDueAmount struct {
										Text       string `xml:",chardata"`
										Nil        string `xml:"nil,attr"`
										C          string `xml:"c,attr"`
										Currency   string `xml:"Currency"`
										LocalValue string `xml:"LocalValue"`
										Value      string `xml:"Value"`
									} `xml:"PastDueAmount"`
									PastDueDays struct {
										Text string `xml:",chardata"`
										Nil  string `xml:"nil,attr"`
									} `xml:"PastDueDays"`
								} `xml:"CalendarItem"`
							} `xml:"PaymentCalendarList"`
							Penalty struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"Penalty"`
							PhaseOfContract  string `xml:"PhaseOfContract"`
							PrincipalArrears struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"PrincipalArrears"`
							PrincipalArrearsFrequency string `xml:"PrincipalArrearsFrequency"`
							PrincipalBalance          struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"PrincipalBalance"`
							ProjectLocation struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"ProjectLocation"`
							ProjectValue struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Nil        string `xml:"nil,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"ProjectValue"`
							ProlongationCounter struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"ProlongationCounter"`
							PurposeOfFinancing string `xml:"PurposeOfFinancing"`
							RealEndDate        struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"RealEndDate"`
							RelatedSubjectsList string `xml:"RelatedSubjectsList"`
							RestructuredCount   struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"RestructuredCount"`
							RestructuringDate struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"RestructuringDate"`
							RestructuringReason string `xml:"RestructuringReason"`
							RoleOfClient        string `xml:"RoleOfClient"`
							StartDate           string `xml:"StartDate"`
							SyndicatedLoan      string `xml:"SyndicatedLoan"`
							TotalAmount         struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"TotalAmount"`
							TotalFacilityAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"TotalFacilityAmount"`
							TotalTakenAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"TotalTakenAmount"`
							WorstPastDueAmount struct {
								Text       string `xml:",chardata"`
								C          string `xml:"c,attr"`
								Currency   string `xml:"Currency"`
								LocalValue string `xml:"LocalValue"`
								Value      string `xml:"Value"`
							} `xml:"WorstPastDueAmount"`
							WorstPastDueDays string `xml:"WorstPastDueDays"`
						} `xml:"Contract"`
					} `xml:"ContractList"`
				} `xml:"Contracts"`
				CurrentRelations struct {
					Text                 string `xml:",chardata"`
					B                    string `xml:"b,attr"`
					ContractRelationList string `xml:"ContractRelationList"`
					InvolvementList      string `xml:"InvolvementList"`
					RelatedPartyList     struct {
						Text         string `xml:",chardata"`
						RelatedParty struct {
							Text    string `xml:",chardata"`
							Contact struct {
								Text      string `xml:",chardata"`
								C         string `xml:"c,attr"`
								FixedLine struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"FixedLine"`
								MobilePhone struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"MobilePhone"`
							} `xml:"Contact"`
							FullName string `xml:"FullName"`
							Gender   string `xml:"Gender"`
							IDNumber struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"IDNumber"`
							IDNumberType string `xml:"IDNumberType"`
							IdScoreId    string `xml:"IdScoreId"`
							LTV          struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"LTV"`
							MainAddress struct {
								Text        string `xml:",chardata"`
								C           string `xml:"c,attr"`
								AddressLine string `xml:"AddressLine"`
								City        struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"City"`
								Country  string `xml:"Country"`
								District struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"District"`
								Parish struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"Parish"`
								PostalCode struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"PostalCode"`
								Street struct {
									Text string `xml:",chardata"`
									Nil  string `xml:"nil,attr"`
								} `xml:"Street"`
							} `xml:"MainAddress"`
							OwnershipShare struct {
								Text string `xml:",chardata"`
								Nil  string `xml:"nil,attr"`
							} `xml:"OwnershipShare"`
							SubjectStatus  string `xml:"SubjectStatus"`
							SubjectType    string `xml:"SubjectType"`
							TypeOfRelation string `xml:"TypeOfRelation"`
							ValidFrom      string `xml:"ValidFrom"`
						} `xml:"RelatedParty"`
					} `xml:"RelatedPartyList"`
				} `xml:"CurrentRelations"`
				Dashboard struct {
					Text string `xml:",chardata"`
					B    string `xml:"b,attr"`
					CIQ  struct {
						Text                  string `xml:",chardata"`
						FraudAlerts           string `xml:"FraudAlerts"`
						FraudAlertsThirdParty string `xml:"FraudAlertsThirdParty"`
					} `xml:"CIQ"`
					Collaterals struct {
						Text                        string `xml:",chardata"`
						NumberOfCollaterals         string `xml:"NumberOfCollaterals"`
						TotalNonCashCollateralValue struct {
							Text       string `xml:",chardata"`
							C          string `xml:"c,attr"`
							Currency   string `xml:"Currency"`
							LocalValue string `xml:"LocalValue"`
							Value      string `xml:"Value"`
						} `xml:"TotalNonCashCollateralValue"`
					} `xml:"Collaterals"`
					Disputes struct {
						Text                            string `xml:",chardata"`
						ActiveContractDisputes          string `xml:"ActiveContractDisputes"`
						FalseDisputes                   string `xml:"FalseDisputes"`
						NumberOfCourtRegisteredDisputes string `xml:"NumberOfCourtRegisteredDisputes"`
					} `xml:"Disputes"`
					Inquiries struct {
						Text                      string `xml:",chardata"`
						InquiriesForLast12Months  string `xml:"InquiriesForLast12Months"`
						SubscribersInLast12Months string `xml:"SubscribersInLast12Months"`
					} `xml:"Inquiries"`
					Involvements struct {
						Text                       string `xml:",chardata"`
						NumberOfActiveInvolvements string `xml:"NumberOfActiveInvolvements"`
					} `xml:"Involvements"`
					OtherLiabilities struct {
						Text                   string `xml:",chardata"`
						NumberOfOpenAgreements string `xml:"NumberOfOpenAgreements"`
					} `xml:"OtherLiabilities"`
					PaymentsProfile struct {
						Text                         string `xml:",chardata"`
						ClosedContracts              string `xml:"ClosedContracts"`
						NumberOfDifferentSubscribers string `xml:"NumberOfDifferentSubscribers"`
						OpenContracts                string `xml:"OpenContracts"`
						PastDueAmountSum             struct {
							Text       string `xml:",chardata"`
							C          string `xml:"c,attr"`
							Currency   string `xml:"Currency"`
							LocalValue string `xml:"LocalValue"`
							Value      string `xml:"Value"`
						} `xml:"PastDueAmountSum"`
						WorstPastDueDaysCurrent         string `xml:"WorstPastDueDaysCurrent"`
						WorstPastDueDaysForLast12Months string `xml:"WorstPastDueDaysForLast12Months"`
					} `xml:"PaymentsProfile"`
					Relations struct {
						Text                 string `xml:",chardata"`
						NumberOfInvolvements string `xml:"NumberOfInvolvements"`
						NumberOfRelations    string `xml:"NumberOfRelations"`
					} `xml:"Relations"`
					Securities struct {
						Text                     string `xml:",chardata"`
						NumberOfActiveSecurities string `xml:"NumberOfActiveSecurities"`
					} `xml:"Securities"`
				} `xml:"Dashboard"`
				Disputes struct {
					Text        string `xml:",chardata"`
					B           string `xml:"b,attr"`
					DisputeList string `xml:"DisputeList"`
					Summary     struct {
						Text                              string `xml:",chardata"`
						NumberOfActiveDisputesContracts   string `xml:"NumberOfActiveDisputesContracts"`
						NumberOfActiveDisputesInCourt     string `xml:"NumberOfActiveDisputesInCourt"`
						NumberOfActiveDisputesSubjectData string `xml:"NumberOfActiveDisputesSubjectData"`
						NumberOfClosedDisputesContracts   string `xml:"NumberOfClosedDisputesContracts"`
						NumberOfClosedDisputesSubjectData string `xml:"NumberOfClosedDisputesSubjectData"`
						NumberOfFalseDisputes             string `xml:"NumberOfFalseDisputes"`
					} `xml:"Summary"`
				} `xml:"Disputes"`
				Financials struct {
					Text                   string `xml:",chardata"`
					B                      string `xml:"b,attr"`
					FinancialStatementList string `xml:"FinancialStatementList"`
				} `xml:"Financials"`
				// Individual struct {
				// 	Text    string `xml:",chardata"`
				// 	B       string `xml:"b,attr"`
				// 	Contact struct {
				// 		Text        string `xml:",chardata"`
				// 		Email       string `xml:"Email"`
				// 		FixedLine   string `xml:"FixedLine"`
				// 		MobilePhone string `xml:"MobilePhone"`
				// 	} `xml:"Contact"`
				// 	General struct {
				// 		Text                       string `xml:",chardata"`
				// 		Alias                      string `xml:"Alias"`
				// 		Citizenship                string `xml:"Citizenship"`
				// 		ClassificationOfIndividual string `xml:"ClassificationOfIndividual"`
				// 		DateOfBirth                string `xml:"DateOfBirth"`
				// 		Education                  string `xml:"Education"`
				// 		EmployerName               string `xml:"EmployerName"`
				// 		EmployerSector             string `xml:"EmployerSector"`
				// 		Employment                 string `xml:"Employment"`
				// 		FullName                   string `xml:"FullName"`
				// 		FullNameLocal              string `xml:"FullNameLocal"`
				// 		Gender                     string `xml:"Gender"`
				// 		MaritalStatus              string `xml:"MaritalStatus"`
				// 		MotherMaidenName           string `xml:"MotherMaidenName"`
				// 		PlaceOfBirth               string `xml:"PlaceOfBirth"`
				// 		Residency                  string `xml:"Residency"`
				// 		SocialStatus               string `xml:"SocialStatus"`
				// 	} `xml:"General"`
				// 	Identifications struct {
				// 		Text                  string `xml:",chardata"`
				// 		IdScoreId             string `xml:"IdScoreId"`
				// 		KTP                   string `xml:"KTP"`
				// 		NPWP                  string `xml:"NPWP"`
				// 		PassportIssuerCountry string `xml:"PassportIssuerCountry"`
				// 		PassportNumber        struct {
				// 			Text string `xml:",chardata"`
				// 			Nil  string `xml:"nil,attr"`
				// 		} `xml:"PassportNumber"`
				// 	} `xml:"Identifications"`
				// 	MainAddress struct {
				// 		Text        string `xml:",chardata"`
				// 		AddressLine string `xml:"AddressLine"`
				// 		City        struct {
				// 			Text string `xml:",chardata"`
				// 			Nil  string `xml:"nil,attr"`
				// 		} `xml:"City"`
				// 		Country  string `xml:"Country"`
				// 		District struct {
				// 			Text string `xml:",chardata"`
				// 			Nil  string `xml:"nil,attr"`
				// 		} `xml:"District"`
				// 		Parish struct {
				// 			Text string `xml:",chardata"`
				// 			Nil  string `xml:"nil,attr"`
				// 		} `xml:"Parish"`
				// 		PostalCode struct {
				// 			Text string `xml:",chardata"`
				// 			Nil  string `xml:"nil,attr"`
				// 		} `xml:"PostalCode"`
				// 		Street struct {
				// 			Text string `xml:",chardata"`
				// 			Nil  string `xml:"nil,attr"`
				// 		} `xml:"Street"`
				// 	} `xml:"MainAddress"`
				// } `xml:"Individual"`
				// Inquiries struct {
				// 	Text        string `xml:",chardata"`
				// 	B           string `xml:"b,attr"`
				// 	InquiryList struct {
				// 		Text    string `xml:",chardata"`
				// 		Inquiry []struct {
				// 			Text           string `xml:",chardata"`
				// 			DateOfInquiry  string `xml:"DateOfInquiry"`
				// 			Product        string `xml:"Product"`
				// 			Reason         string `xml:"Reason"`
				// 			Sector         string `xml:"Sector"`
				// 			SubscriberInfo string `xml:"SubscriberInfo"`
				// 		} `xml:"Inquiry"`
				// 	} `xml:"InquiryList"`
				// 	Summary struct {
				// 		Text                          string `xml:",chardata"`
				// 		NumberOfInquiriesLast12Months string `xml:"NumberOfInquiriesLast12Months"`
				// 		NumberOfInquiriesLast1Month   string `xml:"NumberOfInquiriesLast1Month"`
				// 		NumberOfInquiriesLast24Months string `xml:"NumberOfInquiriesLast24Months"`
				// 		NumberOfInquiriesLast3Months  string `xml:"NumberOfInquiriesLast3Months"`
				// 		NumberOfInquiriesLast6Months  string `xml:"NumberOfInquiriesLast6Months"`
				// 	} `xml:"Summary"`
				// } `xml:"Inquiries"`
				// Involvements struct {
				// 	Text            string `xml:",chardata"`
				// 	B               string `xml:"b,attr"`
				// 	InvolvementList string `xml:"InvolvementList"`
				// 	Summary         struct {
				// 		Text                            string `xml:",chardata"`
				// 		NumberOfActiveInvolvements      string `xml:"NumberOfActiveInvolvements"`
				// 		NumberOfPastInvolvements        string `xml:"NumberOfPastInvolvements"`
				// 		TotalAmountOfActiveInvolvements struct {
				// 			Text       string `xml:",chardata"`
				// 			C          string `xml:"c,attr"`
				// 			Currency   string `xml:"Currency"`
				// 			LocalValue string `xml:"LocalValue"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"TotalAmountOfActiveInvolvements"`
				// 	} `xml:"Summary"`
				// } `xml:"Involvements"`
				// OtherLiabilities struct {
				// 	Text               string `xml:",chardata"`
				// 	B                  string `xml:"b,attr"`
				// 	OtherLiabilityList string `xml:"OtherLiabilityList"`
				// 	Summary            struct {
				// 		Text                     string `xml:",chardata"`
				// 		NumberOfClosedAgreements string `xml:"NumberOfClosedAgreements"`
				// 		NumberOfOpenAgreements   string `xml:"NumberOfOpenAgreements"`
				// 		TotalMarketValue         struct {
				// 			Text       string `xml:",chardata"`
				// 			C          string `xml:"c,attr"`
				// 			Currency   string `xml:"Currency"`
				// 			LocalValue string `xml:"LocalValue"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"TotalMarketValue"`
				// 		TotalPrincipalArrears struct {
				// 			Text       string `xml:",chardata"`
				// 			C          string `xml:"c,attr"`
				// 			Currency   string `xml:"Currency"`
				// 			LocalValue string `xml:"LocalValue"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"TotalPrincipalArrears"`
				// 	} `xml:"Summary"`
				// } `xml:"OtherLiabilities"`
				// Parameters struct {
				// 	Text          string `xml:",chardata"`
				// 	Consent       string `xml:"Consent"`
				// 	IDNumber      string `xml:"IDNumber"`
				// 	IDNumberType  string `xml:"IDNumberType"`
				// 	InquiryReason string `xml:"InquiryReason"`
				// 	ReferenceCode string `xml:"ReferenceCode"`
				// 	ReportDate    string `xml:"ReportDate"`
				// 	Sections      struct {
				// 		Text   string `xml:",chardata"`
				// 		B      string `xml:"b,attr"`
				// 		String string `xml:"string"`
				// 	} `xml:"Sections"`
				// 	SubjectType string `xml:"SubjectType"`
				// } `xml:"Parameters"`
				// ReportInfo struct {
				// 	Text            string `xml:",chardata"`
				// 	B               string `xml:"b,attr"`
				// 	Created         string `xml:"Created"`
				// 	ReferenceNumber string `xml:"ReferenceNumber"`
				// 	ReportStatus    string `xml:"ReportStatus"`
				// 	RequestedBy     string `xml:"RequestedBy"`
				// 	Version         string `xml:"Version"`
				// } `xml:"ReportInfo"`
				// Securities struct {
				// 	Text         string `xml:",chardata"`
				// 	B            string `xml:"b,attr"`
				// 	SecurityList string `xml:"SecurityList"`
				// 	Summary      struct {
				// 		Text                     string `xml:",chardata"`
				// 		NumberOfActiveSecurities string `xml:"NumberOfActiveSecurities"`
				// 		NumberOfPastSecurities   string `xml:"NumberOfPastSecurities"`
				// 		TotalMarketValue         struct {
				// 			Text       string `xml:",chardata"`
				// 			C          string `xml:"c,attr"`
				// 			Currency   string `xml:"Currency"`
				// 			LocalValue string `xml:"LocalValue"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"TotalMarketValue"`
				// 		TotalPrincipalArrears struct {
				// 			Text       string `xml:",chardata"`
				// 			C          string `xml:"c,attr"`
				// 			Currency   string `xml:"Currency"`
				// 			LocalValue string `xml:"LocalValue"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"TotalPrincipalArrears"`
				// 	} `xml:"Summary"`
				// } `xml:"Securities"`
				// SubjectInfoHistory struct {
				// 	Text        string `xml:",chardata"`
				// 	B           string `xml:"b,attr"`
				// 	AddressList struct {
				// 		Text    string `xml:",chardata"`
				// 		Address []struct {
				// 			Text       string `xml:",chardata"`
				// 			Item       string `xml:"Item"`
				// 			Subscriber string `xml:"Subscriber"`
				// 			ValidFrom  string `xml:"ValidFrom"`
				// 			ValidTo    string `xml:"ValidTo"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"Address"`
				// 	} `xml:"AddressList"`
				// 	ContactList struct {
				// 		Text    string `xml:",chardata"`
				// 		Contact []struct {
				// 			Text       string `xml:",chardata"`
				// 			Item       string `xml:"Item"`
				// 			Subscriber string `xml:"Subscriber"`
				// 			ValidFrom  string `xml:"ValidFrom"`
				// 			ValidTo    string `xml:"ValidTo"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"Contact"`
				// 	} `xml:"ContactList"`
				// 	GeneralList struct {
				// 		Text    string `xml:",chardata"`
				// 		General []struct {
				// 			Text       string `xml:",chardata"`
				// 			Item       string `xml:"Item"`
				// 			Subscriber string `xml:"Subscriber"`
				// 			ValidFrom  string `xml:"ValidFrom"`
				// 			ValidTo    string `xml:"ValidTo"`
				// 			Value      string `xml:"Value"`
				// 		} `xml:"General"`
				// 	} `xml:"GeneralList"`
				// 	IdentificationsList string `xml:"IdentificationsList"`
				// } `xml:"SubjectInfoHistory"`
			} `xml:"GetCustomReportResult"`
		} `xml:"GetCustomReportResponse"`
	} `xml:"Body"`
}
