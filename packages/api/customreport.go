package api

import (
	"bytes"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/bank-raya/soap-client/models"
)

func GetReportHandler(c *fiber.Ctx) error {
	Username := os.Getenv("USERNAME")
	Password := os.Getenv("PASSWORD")
	Credentials := Username + ":" + Password
	base64Credentials := base64.StdEncoding.EncodeToString([]byte(Credentials))

	url2 := os.Getenv("CUSTOM_REPORT_ENDPOINT")
	method2 := "POST"

	// SOAP payload
	requestReport := models.CostumReportRequest{
		XmlnsSoapenv: os.Getenv("SCHEMA_XML_SOAP"),
		XmlnsCb5:     os.Getenv("XML_CB5"),
		XmlnsCus:     os.Getenv("XML_CUSTOM_REPORT"),
		XmlnsArr:     os.Getenv("CUS_REPORT_SCHEMA_ARRAY"),
		CostumHeader: models.CostumReportHeader{},
		CostumBody: models.CostumReportBody{
			GetCustomReport: models.GetCustomReport{
				Parameters: models.Parameters{
					Consent:                   true,
					CostumReportIDNumber:      c.Query("IdScoreId"), // Assuming IdScoreId is passed as a query parameter
					CostumReportIDNumberType:  "IdScoreId",
					CostumReportInquiryReason: "ProvidingFacilities",
					CostumReportReferenceCode: "A123",
					CostumReportReportDate:    "2023-06-22",
					CostumSections: models.CostumSections{
						String: []string{"IdScorePlus"},
					},
					SubjectType: "Individual",
				},
			},
		},
	}

	outputReport, err := xml.MarshalIndent(requestReport, "  ", "    ")
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("error: %v", err))
	}

	bodyReport := []byte(xml.Header + string(outputReport))

	reqReport, err := http.NewRequest(method2, url2, bytes.NewBuffer(bodyReport))
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error creating request: %v", err))
	}

	reqReport.Header.Set("Content-Type", "text/xml")
	reqReport.Header.Set("Authorization", "Basic "+base64Credentials)
	reqReport.Header.Set("soapaction", os.Getenv("SOAP_ACTION_CUS_REPORT"))

	client2 := &http.Client{}
	respReport, err := client2.Do(reqReport)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("error: %v", err))
	}
	defer respReport.Body.Close()

	resBody, _ := ioutil.ReadAll(respReport.Body)

	var resReportData models.ResponseReportData
	err = xml.Unmarshal(resBody, &resReportData)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error unmarshalling XML: %v", err))
	}

	return c.JSON(resReportData.Body.GetCustomReportResponse.GetCustomReportResult.CIP.RecordList.Record)

}
