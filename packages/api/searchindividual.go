package api

import (
	"bytes"
	"encoding/base64"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/bank-raya/soap-client/models"
)

func SingleRequest(c *fiber.Ctx) error {
	Username := os.Getenv("USERNAME")
	Password := os.Getenv("PASSWORD")
	Credentials := Username + ":" + Password
	base64Credentials := base64.StdEncoding.EncodeToString([]byte(Credentials))

	url := os.Getenv("SINGLE_REQUEST")
	method := "POST"

	// SOAP payload
	envelope := models.SingleRequest{
		XmlnsSoapenv: os.Getenv("SCHEMA_XML_SOAP"),
		XmlnsCb5:     os.Getenv("XML_CB5"),
		XmlnsSmar:    os.Getenv("SMART_SEARCH"),
		Header:       models.SingleHeader{},
		Body: models.SingleBody{
			SmartSearchIndividual: models.SingleSmartSearchIndividual{
				Query: models.SingleQuery{
					InquiryReason: "ProvidingFacilities",
					Parameters: models.SingleParameters{
						DateOfBirth: "1988-02-29",
						FullName:    "Bima Sakti",
						IdNumbers: models.SingleIdNumbers{
							IdNumberPairIndividual: models.SingleIdNumberPairIndividual{
								IdNumber:     "3150972902880002",
								IdNumberType: "KTP",
							},
						},
					},
					ReferenceCode: "A123",
				},
			},
		},
	}

	output, err := xml.MarshalIndent(envelope, "  ", "    ")
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("error: %v", err))
	}

	body := []byte(xml.Header + string(output))

	// Create a new request
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("Error creating request: %v", err))
	}

	// Set headers
	req.Header.Set("Content-Type", "text/xml")
	req.Header.Set("Authorization", "Basic "+base64Credentials)
	req.Header.Set("soapaction", os.Getenv("SOAP_ACTION_INDIVIDUAL"))

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("error: %v", err))
	}
	defer resp.Body.Close()

	responseBody, _ := ioutil.ReadAll(resp.Body)

	var responseReport models.SingleResponse
	err = xml.Unmarshal(responseBody, &responseReport)
	if err != nil {
		return c.Status(fiber.StatusInternalServerError).SendString(fmt.Sprintf("error: %v", err))
	}
	response := responseReport.Body.SmartSearchIndividualResponse.SmartSearchIndividualResult

	return c.JSON(response)

}
