package main

import (
	"log"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
	"gitlab.com/bank-raya/soap-client/packages/api"
)

func main() {
	// Credentials and URL setup
	godotenv.Load(".env")

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.Status(200).JSON(fiber.Map{
			"message": "Hello",
		})
	})

	app.Post("/single-request", api.SingleRequest)
	app.Post("/get-report", api.GetReportHandler)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8004" // Default port if not set
	}

	log.Fatal(app.Listen(":" + port))

	// Result parsing
	// fmt.Println("You have score is : ", resReportData.Body.GetCustomReportResponse.GetCustomReportResult.CIP.RecordList.Record[1].Score)
	// for _, record := range resReportData.Body.GetCustomReportResponse.GetCustomReportResult.CIP.RecordList.Record {
	// 	fmt.Printf("Date: %s\n", record.Date)
	// 	fmt.Printf("Grade: %s\n", record.Grade)
	// 	fmt.Printf("Score: %s\n", record.Score)
	// 	for _, reason := range record.ReasonsList.Reason {
	// 		fmt.Printf("Reason Code: %s\n", reason.Code)
	// 		fmt.Printf("Reason Description: %s\n", reason.Description)
	// 	}
	// }
}
